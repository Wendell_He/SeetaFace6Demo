/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
/* Header for class com_seetaface_v6_FaceAntiSpoofing */

#ifndef _Included_com_seetaface_v6_FaceAntiSpoofing
#define _Included_com_seetaface_v6_FaceAntiSpoofing
#ifdef __cplusplus
extern "C" {
#endif
#undef com_seetaface_v6_FaceAntiSpoofing_STATUS_REAL
#define com_seetaface_v6_FaceAntiSpoofing_STATUS_REAL 0L
#undef com_seetaface_v6_FaceAntiSpoofing_STATUS_SPOOF
#define com_seetaface_v6_FaceAntiSpoofing_STATUS_SPOOF 1L
#undef com_seetaface_v6_FaceAntiSpoofing_STATUS_FUZZY
#define com_seetaface_v6_FaceAntiSpoofing_STATUS_FUZZY 2L
#undef com_seetaface_v6_FaceAntiSpoofing_STATUS_DETECTING
#define com_seetaface_v6_FaceAntiSpoofing_STATUS_DETECTING 3L
#undef com_seetaface_v6_FaceAntiSpoofing_PROPERTY_NUMBER_THREADS
#define com_seetaface_v6_FaceAntiSpoofing_PROPERTY_NUMBER_THREADS 4L
#undef com_seetaface_v6_FaceAntiSpoofing_PROPERTY_ARM_CPU_MODE
#define com_seetaface_v6_FaceAntiSpoofing_PROPERTY_ARM_CPU_MODE 5L
#undef com_seetaface_v6_FaceAntiSpoofing_ANTI_TYPE_PARTIAL
#define com_seetaface_v6_FaceAntiSpoofing_ANTI_TYPE_PARTIAL 0L
#undef com_seetaface_v6_FaceAntiSpoofing_ANTI_TYPE_TOTALLY
#define com_seetaface_v6_FaceAntiSpoofing_ANTI_TYPE_TOTALLY 1L
/*
 * Class:     com_seetaface_v6_FaceAntiSpoofing
 * Method:    nativeCreateEngine
 * Signature: (Ljava/lang/String;Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_com_seetaface_v6_FaceAntiSpoofing_nativeCreateEngine
  (JNIEnv *, jobject, jstring, jstring);

/*
 * Class:     com_seetaface_v6_FaceAntiSpoofing
 * Method:    nativeDestroyEngine
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_com_seetaface_v6_FaceAntiSpoofing_nativeDestroyEngine
  (JNIEnv *, jobject);

/*
 * Class:     com_seetaface_v6_FaceAntiSpoofing
 * Method:    nativePredict
 * Signature: (Lcom/seetaface/v6/SeetaImageData;Lcom/seetaface/v6/SeetaRect;[Lcom/seetaface/v6/SeetaPointF;)I
 */
JNIEXPORT jint JNICALL Java_com_seetaface_v6_FaceAntiSpoofing_nativePredict
  (JNIEnv *, jobject, jobject, jobject, jobjectArray);

/*
 * Class:     com_seetaface_v6_FaceAntiSpoofing
 * Method:    nativePredictVideo
 * Signature: (Lcom/seetaface/v6/SeetaImageData;Lcom/seetaface/v6/SeetaRect;[Lcom/seetaface/v6/SeetaPointF;)I
 */
JNIEXPORT jint JNICALL Java_com_seetaface_v6_FaceAntiSpoofing_nativePredictVideo
  (JNIEnv *, jobject, jobject, jobject, jobjectArray);

/*
 * Class:     com_seetaface_v6_FaceAntiSpoofing
 * Method:    nativeResetVideo
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_com_seetaface_v6_FaceAntiSpoofing_nativeResetVideo
  (JNIEnv *, jobject);

/*
 * Class:     com_seetaface_v6_FaceAntiSpoofing
 * Method:    nativeGetPreFrameScore
 * Signature: ([F[F)V
 */
JNIEXPORT void JNICALL Java_com_seetaface_v6_FaceAntiSpoofing_nativeGetPreFrameScore
  (JNIEnv *, jobject, jfloatArray, jfloatArray);

/*
 * Class:     com_seetaface_v6_FaceAntiSpoofing
 * Method:    nativeSetVideoFrameCount
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_com_seetaface_v6_FaceAntiSpoofing_nativeSetVideoFrameCount
  (JNIEnv *, jobject, jint);

/*
 * Class:     com_seetaface_v6_FaceAntiSpoofing
 * Method:    nativeGetVideoFrameCount
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_com_seetaface_v6_FaceAntiSpoofing_nativeGetVideoFrameCount
  (JNIEnv *, jobject);

/*
 * Class:     com_seetaface_v6_FaceAntiSpoofing
 * Method:    nativeSetThreshold
 * Signature: (FF)V
 */
JNIEXPORT void JNICALL Java_com_seetaface_v6_FaceAntiSpoofing_nativeSetThreshold
  (JNIEnv *, jobject, jfloat, jfloat);

/*
 * Class:     com_seetaface_v6_FaceAntiSpoofing
 * Method:    nativeGetThreshold
 * Signature: (Ljava/lang/Float;Ljava/lang/Float;)V
 */
JNIEXPORT void JNICALL Java_com_seetaface_v6_FaceAntiSpoofing_nativeGetThreshold
  (JNIEnv *, jobject, jobject, jobject);

/*
 * Class:     com_seetaface_v6_FaceAntiSpoofing
 * Method:    nativeSetProperty
 * Signature: (ID)V
 */
JNIEXPORT void JNICALL Java_com_seetaface_v6_FaceAntiSpoofing_nativeSetProperty
  (JNIEnv *, jobject, jint, jdouble);

/*
 * Class:     com_seetaface_v6_FaceAntiSpoofing
 * Method:    nativeGetProperty
 * Signature: (I)D
 */
JNIEXPORT jdouble JNICALL Java_com_seetaface_v6_FaceAntiSpoofing_nativeGetProperty
  (JNIEnv *, jobject, jint);

#ifdef __cplusplus
}
#endif
#endif
