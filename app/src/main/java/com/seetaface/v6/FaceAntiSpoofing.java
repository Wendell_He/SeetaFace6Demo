package com.seetaface.v6;

import android.util.Log;

public class FaceAntiSpoofing {

    private static final String TAG = "FaceAntiSpoofing";

    private static final int STATUS_REAL = 0;   ///< 真实人脸
    private static final int STATUS_SPOOF = 1;  ///< 攻击人脸（假人脸）
    private static final int STATUS_FUZZY = 2;  ///< 无法判断（人脸成像质量不好）
    private static final int STATUS_DETECTING = 3;  ///< 正在检测

    public static final int PROPERTY_NUMBER_THREADS = 4;
    public static final int PROPERTY_ARM_CPU_MODE = 5;

    public static final int ANTI_TYPE_PARTIAL = 0;
    public static final int ANTI_TYPE_TOTALLY = 1;

    public static final String MODEL_FIRST = "fas_first.csta";
    public static final String MODEL_SECOND = "fas_second.csta";

    static {
        System.loadLibrary("FaceAntiSpoofing");
    }

    public FaceAntiSpoofing(int antiType) {
        String firstModel = null;
        String secondModel = null;
        if (antiType == ANTI_TYPE_TOTALLY) {
            firstModel = SeetaUtils.getSeetaFaceModelPath(MODEL_FIRST);
            secondModel = SeetaUtils.getSeetaFaceModelPath(MODEL_SECOND);
        } else {
            firstModel = SeetaUtils.getSeetaFaceModelPath(MODEL_FIRST);
        }
        int result = nativeCreateEngine(firstModel, secondModel);
        Log.d(TAG, "-------gu--- createEngine result:" + result);
        nativeSetProperty(PROPERTY_NUMBER_THREADS, 1);
    }

    public void destroy() {
        int result = nativeDestroyEngine();
        Log.d(TAG, "-------gu--- destroyEngine result:" + result);
    }

    private native int nativeCreateEngine(String firstModel, String secondModel);
    private native int nativeDestroyEngine();

    private native int nativePredict(SeetaImageData imageData, SeetaRect rect, SeetaPointF[] pointFs);
    private native int nativePredictVideo(SeetaImageData imageData, SeetaRect rect, SeetaPointF[] pointFs);

    private native void nativeResetVideo();
    private native void nativeGetPreFrameScore(float[] clarity, float[] reality);

    private native void nativeSetVideoFrameCount(int number);
    private native int nativeGetVideoFrameCount();

    private native void nativeSetThreshold(float clarity, float reality);
    private native void nativeGetThreshold(Float clarity, Float reality);

    private native void nativeSetProperty(int type, double value);
    private native double nativeGetProperty(int type);
}
