package com.seetaface.v6;

public class QualityChecker {

    public static final int QUALITY_BRIGHTNESS = 0; // 非深度学习的人脸亮度评估器
    public static final int QUALITY_CLARITY = 1; // 非深度学习的人脸清晰度评估器
    public static final int QUALITY_LBN = 2; // 深度学习的人脸清晰度评估器
    public static final int QUALITY_POSE = 3; // 非深度学习的人脸姿态评估器
    public static final int QUALITY_POSE_EX = 4; // 深度学习的人脸姿态评估器
    public static final int QUALITY_RESOLUTION = 5; // 非深度学习的人脸尺寸评估器

    // LBN
    public static final String MODEL_LBN = "quality_lbn.csta";
    public static final int LBN_PROPERTY_NUMBER_THREADS = 4;
    public static final int LBN_PROPERTY_ARM_CPU_MODE = 5;
    public static final int LBN_PROPERTY_LIGHT_THRESH = 10;
    public static final int LBN_PROPERTY_BLUR_THRESH = 11;
    public static final int LBN_PROPERTY_NOISE_THRESH = 12;

    public static final int LBN_LIGHT_BRIGHT = 0;
    public static final int LBN_LIGHT_DARK = 1;
    public static final int LBN_BLUR_CLEAR = 0;
    public static final int LBN_BLUR_BLUR = 1;
    public static final int LBN_NOISE_HAVE = 0;
    public static final int LBN_NOISE_NO = 1;

    // pose ex
    public static final String MODEL_POSE_EX = "pose_estimation.csta";
    public static final int POSE_EX_PROPERTY_YAW_LOW_THRESHOLD = 0;
    public static final int POSE_EX_PROPERTY_YAW_HIGH_THRESHOLD = 1;
    public static final int POSE_EX_PROPERTY_PITCH_LOW_THRESHOLD = 2;
    public static final int POSE_EX_PROPERTY_PITCH_HIGH_THRESHOLD = 3;
    public static final int POSE_EX_PROPERTY_ROLL_LOW_THRESHOLD = 4;
    public static final int POSE_EX_PROPERTY_ROLL_HIGH_THRESHOLD = 5;

    static {
        System.loadLibrary("QualityChecker");
    }

    public QualityChecker() {
    }

    // Brightness
    private native int nativeStartBrightnessChecker();
    private native int nativeStartBrightnessChecker(float v0, float v1, float v2, float v3);
    private native QualityResult nativeBrightnessCheck(SeetaImageData imageData, SeetaRect rect,
                                                       SeetaPointF[] pointFs, int featureSize);
    private native int nativeStopBrightnessChecker();

    // clarity
    private native int nativeStartClarityChecker();
    private native int nativeStartClarityChecker(float low, float high);
    private native QualityResult nativeClarityCheck(SeetaImageData imageData, SeetaRect rect,
                                                       SeetaPointF[] pointFs, int featureSize);
    private native int nativeStopClarityChecker();

    // LBN
    private native int nativeStartLBNChecker(String model);
    private native void nativeLBNDetect(SeetaImageData imageData, SeetaPointF[] pointFs,
                                                 int[] light, int[] blur, int[] noise);
    private native void nativeLBNSetProperty(int type, double value);
    private native double nativeLBNGetProperty(int type);
    private native int nativeStopLBNChecker();

    // Pose
    private native QualityResult nativePoseCheck(SeetaImageData imageData, SeetaRect rect,
                                       SeetaPointF[] pointFs, int featureSize);

    // PoseEx
    private native int nativeStartPoseExChecker(String model);
    private native QualityResult nativePoseExCheck(SeetaImageData imageData, SeetaRect rect,
                                          SeetaPointF[] pointFs, int featureSize);
    private native void nativePoseExSetProperty(int type, float value);
    private native float nativePoseExGetProperty(int type);
    private native int nativeStopPoseExChecker();

    // Resolution
    private native int nativeStartResolutionChecker();
    private native int nativeStartResolutionChecker(float low, float high);
    private native QualityResult nativeResolutionCheck(SeetaImageData imageData, SeetaRect rect,
                                                    SeetaPointF[] pointFs, int featureSize);
    private native int nativeStopResolutionChecker();

    // Integrity
    private native int nativeStartIntegrityChecker();
    private native int nativeStartIntegrityChecker(float low, float high);
    private native QualityResult nativeIntegrityCheck(SeetaImageData imageData, SeetaRect rect,
                                                       SeetaPointF[] pointFs, int featureSize);
    private native int nativeStopIntegrityChecker();
}
