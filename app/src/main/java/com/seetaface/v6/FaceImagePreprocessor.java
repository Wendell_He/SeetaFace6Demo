package com.seetaface.v6;

import android.graphics.Bitmap;

public class FaceImagePreprocessor {

    static {
        System.loadLibrary("FacePreprocessor");
    }

    private static FaceImagePreprocessor mInstance;

    private FaceImagePreprocessor() {
    }

    public static FaceImagePreprocessor getInstance() {
        if (mInstance == null) {
            mInstance = new FaceImagePreprocessor();
        }
        return mInstance;
    }

    public SeetaImageData getImageDataFromBitmap(Bitmap bitmap) {
        return processBitmap(bitmap);
    }

    /**
     * camera1 NV21 {@link android.graphics.ImageFormat#NV21}
     * @param yuv
     * @param width
     * @param height
     * @param format 0:NV21
     * @return
     */
    public SeetaImageData getImageDataFromYUVData(byte[] yuv, int width, int height, int format) {
        return processYUVData(yuv, width, height, format);
    }

    /**
     * camera2 YUV_420_888 {@link android.graphics.ImageFormat#YUV_420_888}
     * @param y
     * @param u
     * @param v
     * @param width
     * @param height
     * @return
     */
    public SeetaImageData getImageDataFromYUVData(byte[] y, byte[] u, byte[] v, int width, int height) {
        return processSplitYUVData(y, u, v, width, height);
    }

    private native SeetaImageData processBitmap(Bitmap originBitmap);
    private native SeetaImageData processYUVData(byte[] yuv, int width, int height, int format);
    private native SeetaImageData processSplitYUVData(byte[] y, byte[] u, byte[] v, int width, int height);
}
