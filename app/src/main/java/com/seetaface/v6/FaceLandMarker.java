package com.seetaface.v6;

import android.util.Log;

public class FaceLandMarker {

    private static final String TAG = "FaceLandMarker";

    public static final String MODEL_LAND_MARKER_PTS5 = "face_landmarker_pts5.csta";
    public static final String MODEL_LAND_MARKER_PTS68 = "face_landmarker_pts68.csta";
    public static final String MODEL_LAND_MARKER_MASK_PTS5 = "face_landmarker_mask_pts5.csta";

    static {
        System.loadLibrary("FaceLandMarker");
    }

    public FaceLandMarker(String model) {
        String landMarksModel = SeetaUtils.getSeetaFaceModelPath(model);
        int result = nativeCreateEngine(landMarksModel);
        Log.d(TAG, "-------gu--- createEngine result:" + result);
    }

    public void destroy() {
        int result = nativeDestroyEngine();
        Log.d(TAG, "-------gu--- destroyEngine result:" + result);
    }

    public int getFacePointsLength() {
        return nativeGetPointsLength();
    }

    public SeetaPointF[] getImageFacePoints(SeetaImageData image, SeetaRect rect) {
        return nativeGetFacePoints(image, rect);
    }

    public void getImageFacePoints(SeetaImageData image, SeetaRect rect, SeetaPointF[] points) {
        nativeGetFacePoints(image, rect, points);
    }

    public SeetaPointsWithMask[] getImageFacePointsWithMask(SeetaImageData image, SeetaRect rect) {
        return nativeGetFacePointsWithMask(image, rect);
    }

    public void getImageFacePointsWithMask(SeetaImageData image, SeetaRect rect, SeetaPointsWithMask[] pointWithMask) {
        nativeGetFacePointsWithMask(image, rect, pointWithMask);
    }

    private native int nativeCreateEngine(String landMarksModel);
    private native int nativeDestroyEngine();
    private native int nativeGetPointsLength();
    private native SeetaPointF[] nativeGetFacePoints(SeetaImageData image, SeetaRect rect);
    private native void nativeGetFacePoints(SeetaImageData image, SeetaRect rect, SeetaPointF[] pointFS);
    private native SeetaPointsWithMask[] nativeGetFacePointsWithMask(SeetaImageData image, SeetaRect rect);
    private native void nativeGetFacePointsWithMask(SeetaImageData image, SeetaRect rect, SeetaPointsWithMask[] pointWithMask);
}
