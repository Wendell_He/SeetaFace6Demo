package com.seetaface.v6;

public class FaceRecognizer {

    public static final int PROPERTY_NUMBER_THREADS = 4;
    public static final int PROPERTY_ARM_CPU_MODE = 5;

    public static final String MODEL_RECOGNIZER = "face_recognizer.csta";
    public static final String MODEL_RECOGNIZER_LIGHT = "face_recognizer_light.csta";
    public static final String MODEL_RECOGNIZER_MASK = "face_recognizer_mask.csta";

    static {
        System.loadLibrary("FaceRecognizer");
    }

    public FaceRecognizer(String model) {
        String recognizerModel = SeetaUtils.getSeetaFaceModelPath(model);
        nativeCreateEngine(recognizerModel);

        nativeSetProperty(PROPERTY_NUMBER_THREADS, 1);
    }

    public void destroy() {
        nativeDestroyEngine();
    }

    private native int nativeCreateEngine(String recognizeModel);
    private native int nativeDestroyEngine();

    private native int nativeGetCropFaceWidth();
    private native int nativeGetCropFaceHeight();
    private native int nativeGetCropFaceChannels();
    private native boolean nativeCropFace(SeetaImageData image, SeetaPointF[] pointF, SeetaImageData crop);
    private native SeetaImageData nativeCropFace(SeetaImageData image, SeetaPointF[] pointF);

    private native int nativeGetCropFaceWidthV2();
    private native int nativeGetCropFaceHeightV2();
    private native int nativeGetCropFaceChannelsV2();
    private native boolean nativeCropFaceV2(SeetaImageData image, SeetaPointF[] pointF, SeetaImageData croppedImage);
    private native SeetaImageData nativeCropFaceV2(SeetaImageData image, SeetaPointF[] pointF);

    private native int nativeGetExtractFeatureSize();
    private native boolean nativeExtractFace(SeetaImageData image, SeetaPointF[] pointF, float[] features);
    private native boolean nativeExtractCropFace(SeetaImageData croppedImage, float[] features);

    private native float nativeCompareFace(float[] features1, float[] features2);

    private native void nativeSetProperty(int type, double value);
    private native double nativeGetProperty(int type);
}
