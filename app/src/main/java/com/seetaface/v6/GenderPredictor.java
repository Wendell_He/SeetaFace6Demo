package com.seetaface.v6;

import android.util.Log;

public class GenderPredictor {

    private static final String TAG = "GenderPredictor";

    public static final int GENDER_MALE = 0;
    public static final int GENDER_FEMALE = 1;

    public static final int PROPERTY_NUMBER_THREADS = 4;
    public static final int PROPERTY_ARM_CPU_MODE = 5;

    public static final String MODEL_GENDER_PREDICTOR = "gender_predictor.csta";

    static {
        System.loadLibrary("GenderPredictor");
    }

    public GenderPredictor() {
        String genderPredictorModel = SeetaUtils.getSeetaFaceModelPath(MODEL_GENDER_PREDICTOR);
        int result = nativeCreateEngine(genderPredictorModel);
        Log.d(TAG, "-------gu--- createEngine result:" + result);
        nativeSetProperty(PROPERTY_NUMBER_THREADS, 1);
    }

    public void destroy() {
        int result = nativeDestroyEngine();
        Log.d(TAG, "-------gu--- destroyEngine result:" + result);
    }

    private native int nativeCreateEngine(String genderPredictorModel);
    private native int nativeDestroyEngine();

    private native int nativeGetCropFaceWidth();
    private native int nativeGetCropFaceHeight();
    private native int nativeGetCropFaceChannels();
    private native boolean nativeCropFace(SeetaImageData image, SeetaPointF[] pointFs, SeetaImageData crop);

    private native int nativePredictGender(SeetaImageData image);
    private native int nativePredictGenderWithCrop(SeetaImageData image, SeetaPointF[] pointFs);

    private native void nativeSetProperty(int type, double value);
    private native double nativeGetProperty(int type);
}
