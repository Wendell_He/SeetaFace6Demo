package com.seetaface.v6;

import android.util.Log;

public class EyeStateDetector {

    private static final String TAG = "EyeStateDetector";

    public static final int EYE_STATE_CLOSE = 0; // 闭眼
    public static final int EYE_STATE_OPEN = 1; // 睁眼
    public static final int EYE_STATE_RANDOM = 2; // 非眼部区域
    public static final int EYE_STATE_UNKNOWN = 3; // 未知状态

    public static final int PROPERTY_NUMBER_THREADS = 4;
    public static final int PROPERTY_ARM_CPU_MODE = 5;

    public static final String MODEL_EYE_STATE = "eye_state.csta";

    static {
        System.loadLibrary("EyeStateDetector");
    }

    public EyeStateDetector() {
        String eyeStateModel = SeetaUtils.getSeetaFaceModelPath(MODEL_EYE_STATE);
        int result = nativeCreateEngine(eyeStateModel);
        Log.d(TAG, "-------gu--- createEngine result:" + result);
        nativeSetProperty(PROPERTY_NUMBER_THREADS, 1);
    }

    public void destroy() {
        int result = nativeDestroyEngine();
        Log.d(TAG, "-------gu--- destroyEngine result:" + result);
    }

    private native int nativeCreateEngine(String eyeStateModel);
    private native int nativeDestroyEngine();

    private native void nativeEyeStateDetect(SeetaImageData imageData, SeetaPointF[] pointFs,
                                             Integer leftState, Integer rightState);

    private native void nativeSetProperty(int type, double value);
    private native double nativeGetProperty(int type);
}
