package com.gu.seetaface6demo;

import android.graphics.Bitmap;
import android.util.Log;

import com.seetaface.v6.FaceDetector;
import com.seetaface.v6.FaceImagePreprocessor;
import com.seetaface.v6.SeetaFaceInfo;
import com.seetaface.v6.SeetaImageData;

public class TestDetectorEngine {

    public static void test(Bitmap bmp) {
        FaceImagePreprocessor faceImagePreprocessor = FaceImagePreprocessor.getInstance();
        SeetaImageData imageData = faceImagePreprocessor.getImageDataFromBitmap(bmp);
        if (imageData != null) {
            FaceDetector faceDetector = new FaceDetector();
            SeetaFaceInfo[] faceInfos = faceDetector.detectFaces(imageData);
            if (faceInfos != null) {
                Log.d("TestDetectorEngine", "-------gu--- faceInfo_length:" + faceInfos.length);
                for (SeetaFaceInfo faceInfo : faceInfos) {
                    Log.d("TestDetectorEngine", "-------gu--- faceInfo:" + faceInfo);
                }
            }
            faceDetector.destroy();
        }
    }
}
