package com.gu.seetaface6demo;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.seetaface.v6.SeetaUtils;

import java.io.IOException;
import java.io.InputStream;

public class MainActivity extends PermissionActivity implements View.OnClickListener {

    private static final String TAG = "MainActivity";

    private View mProgressLayout;

    private View mActionLayout;
    private ImageView mPicView;
    private Bitmap mPicBitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mProgressLayout = findViewById(R.id.progress_layout);

        mActionLayout = findViewById(R.id.action_layout);
        mPicView = findViewById(R.id.pic_view);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        recyclePicBitmap();
    }

    @Override
    protected void notifyAllPermissionsRequested() {
        if (SeetaUtils.shouldCopyAssetsModels(MainActivity.this)) {
            Log.d(TAG, "-------gu--- notifyAllPermissionsRequested model not exist should copy");
            mActionLayout.setVisibility(View.GONE);
            mProgressLayout.setVisibility(View.VISIBLE);
            new Thread(() -> {
                SeetaUtils.copyModelsToExternalStorage(MainActivity.this);
                showActionLayoutAndPicture();
            }).start();
        } else {
            Log.d(TAG, "-------gu--- notifyAllPermissionsRequested");
            showActionLayoutAndPicture();
        }
    }

    private void recyclePicBitmap() {
        if (mPicBitmap != null && !mPicBitmap.isRecycled()) {
            mPicBitmap.recycle();
            mPicBitmap = null;
        }
    }

    private void showActionLayoutAndPicture() {
        runOnUiThread(() -> {
            mActionLayout.setVisibility(View.VISIBLE);
            mProgressLayout.setVisibility(View.GONE);

            recyclePicBitmap();
            mPicBitmap = getAssetsPicture("pic.jpg");
            mPicView.setImageBitmap(mPicBitmap);
        });
    }

    private Bitmap getAssetsPicture(String fileName) {
        Bitmap bitmap = null;
        AssetManager assetManager = getAssets();
        InputStream is = null;
        try {
            is = assetManager.open(fileName);
            bitmap = BitmapFactory.decodeStream(is);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return bitmap;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.action_button) {
            TestDetectorEngine.test(mPicBitmap);
        }
    }

}