package com.gu.seetaface6demo;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;

import java.util.ArrayList;

import androidx.annotation.NonNull;

public class PermissionActivity extends Activity {

    private static final String TAG = "PermissionActivity";

    public static int PERMISSION_REQUEST_CODE = 0x123456;

    private String[] NEED_REQUEST_PERMISSIONS = new String[] {
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
    };

    public boolean mAllPermissionsGranted = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        ArrayList<String> needRequestPermissionList = new ArrayList<>();
        for (String permission : NEED_REQUEST_PERMISSIONS) {
            if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                Log.d(TAG, "-------gu--- onCreate not granted permission:" + permission);
                needRequestPermissionList.add(permission);
            }
        }
        if (needRequestPermissionList.size() > 0) {
            String[] needRequestPermissions = needRequestPermissionList.toArray(new String[0]);
            requestPermissions(needRequestPermissions, PERMISSION_REQUEST_CODE);
        } else {
            mAllPermissionsGranted = true;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mAllPermissionsGranted) {
            notifyAllPermissionsRequested();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_REQUEST_CODE) {
            /*if (permissions == null) {
                return;
            }
            for (int i = 0; i < permissions.length; i++) {
                Log.d(TAG, "-------gu--- permission:" + permissions[i]
                        + ", grantResult:" + (grantResults[i] == PackageManager.PERMISSION_GRANTED ? "granted" : "not granted"));

            }*/
            boolean allPermissionsRequested = true;
            for (String permission : NEED_REQUEST_PERMISSIONS) {
                if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                    Log.d(TAG, "-------gu--- onRequestPermissionsResult not granted permission:" + permission);
                    allPermissionsRequested = false;
                }
            }
            if (allPermissionsRequested) {
                mAllPermissionsGranted = true;
                notifyAllPermissionsRequested();
            }
        }
    }

    protected void notifyAllPermissionsRequested() {}

}